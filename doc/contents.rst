
.. currentmodule:: tango

.. _contents:

========
Contents
========

.. toctree::
    :maxdepth: 2
    :titlesonly:

    installation
    tutorial
    how-to
    API reference <api>
    News and releases <versions_news>
    TEP <tep>
    Index <genindex>

**Last update:** |today|
