
.. currentmodule:: tango

.. _pytango-api:

===========
PyTango API
===========

Here you can find full PyTango API

.. toctree::
    :maxdepth: 1

    api/data_types
    api/client_api/index
    api/server_api/index
    api/database
    api/encoded
    api/utilities
    api/exception
