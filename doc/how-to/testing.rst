.. currentmodule:: tango

.. _testing:

=======================
Testing PyTango Devices
=======================

.. toctree::
    :maxdepth: 1

    testing/testing_approaches
    testing/test_context
    testing/mocks
    testing/coverage
